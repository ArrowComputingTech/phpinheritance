<?php

  class Animal {
    protected $name;
    protected $hunts;
    
    final function bleeds() {
      echo "There is red everywhere!";
    }

    function hunt() {
      if ($this->hunts) {
        echo "This animal hunts.<br>";
      } else {
        echo "This animal does not hunt.<br>";
      }
    }
  }

  class Dog extends Animal {
    function __construct($name, $hunts) {
      $this->name = $name;
      $this->hunts = $hunts;
    }

    function makeSound() {
      echo "Woof woof!<br>";
    }

    function showName() {
      echo "$this->name is the animal's name.<br>";
      echo "$this->name is a " . static::class . "<br>";
    }
  }

  class Tiger extends Animal {
    function __construct($name, $hunts) {
      $this->name = $name;
      $this->hunts = $hunts;
    }

    function makeSound() {
      echo "ROOOAAARR!<br>";
    }

    function showName() {
      echo "$this->name is the animal's name.<br>";
      echo "$this->name is a " . static::class . "<br>";
    }
  }

  $dog1 = new Dog("Gypsy", false);
  $dog1->makeSound();
  $dog1->hunt();
  $dog1->showName();
  echo "<br><br>";
  $tiger1 = new Tiger("Lulu", true);
  $tiger1->makeSound();
  $tiger1->hunt();
  $tiger1->showName();
?>
